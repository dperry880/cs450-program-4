; The layout used for nodes is in the following order:
;
; (smallestValue, largestValue, smallest list, middle
; list, largest list)
;
; This way, the location of each value will always be
; consistant even if it is a leaf node containing
; no lists. The following 5 functions are used to 
; get any of the fields related to a node.
(defun getSmallVal (node) 
  (car node)
)

(defun getLargeVal (node) 
  (car (cdr node))
)

(defun getSmallList (node) 
  (cond
    ((<= (length node) 2)
     nil
    )
    (t (car (cdr (cdr node))))
  )
)

(defun getMidList (node) 
  (cond
    ((<= (length node) 2)
     nil
    )
    (t (car (cdr (cdr (cdr node)))))
  )
)

(defun getLargeList (node) 
  (cond
    ((<= (length node) 2)
     nil
    )
    (t (car (cdr (cdr (cdr (cdr node))))))
  )
)

; Function for converting the format for
; the node I used to match the specification.
(defun outputForm (node)
  (cond
    ((eq node nil)
     node
    )
    ((<= (length node) 2)
     node
    )
    (t 
     (cons (outputForm (getSmallList node)) 
     (cons (getSmallVal node) 
     (cons (outputForm (getMidList node)) 
     (cons (getLargeVal node) 
     (cons (outputForm (getLargeList node)) nil))))
     )
    )
  )
)

; Recursive function for inserting a value into
; a ternary tree.
(defun insert (inNode toInsertList)
  (cond
    ; if there are no more values to insert,
    ; return the input node
    ((eq nil (first toInsertList)) inNode)
    ; if the input node is an empty node
    ((eq (length inNode) 0) 
      ; recursively call insert on a node whos small
      ; value is the value to be inserted
      (insert (cons (first toInsertList) nil) (cdr toInsertList))
    )
    ; if the input node has one value
    ((eq (length inNode) 1) 
      ; recursively call insert on a node whos small Value
      ; is the smallest value inserted, and the large Value
      ; is the largest value.
      (insert 
	(cons (min (getSmallVal inNode) (first toInsertList)) 
	(cons (max (getSmallVal inNode) (first toInsertList)) nil)) 
	(cdr toInsertList)
      )
    )
    ; if the node has its large and small values set
    ((>= (length inNode) 2)
      (cond
	; if the value to insert is smaller than the small value
	((<= (first toInsertList) (getSmallVal inNode))
	  (cond
	    ; if the length of the node's small list is 0
	    ((< (length (getSmallList inNode)) 1)
	      ; recursively call insert on a node who has
	      ; the same small and large values as the original
	      ; node, but with a list of small values added.
	      (insert 
		(cons (getSmallVal inNode) 
		(cons (getLargeVal inNode) 
		(cons (insert () (cons (first toInsertList) nil)) 
		(cons (getMidList inNode) 
		(cons (getLargeList inNode) nil))))) 
		(cdr toInsertList)
	      ) 
	    )
	    ; if the length of the small list is not 0
	    ((>= (length (getSmallList inNode)) 1)
	      ; recursively call insert on a node who has
	      ; the same small and large values as the original
	      ; node, but with a list of small values added.
	      (insert 
		(cons (getSmallVal inNode) 
		(cons (getLargeVal inNode) 
		(cons (insert (getSmallList inNode) 
		(cons (first toInsertList) nil)) 
		(cons (getMidList inNode) 
		(cons (getLargeList inNode) nil))))) 
		(cdr toInsertList)
	      ) 
	    )
	  )
	)
	; if the value to insert is inbetween the small and
	; large values.
	((<= (first toInsertList) (getLargeVal inNode))
	  (cond 
	    ; if the length of the mid list is 0
	    ((< (length (getMidList inNode)) 1)
	      ; recursively call insert on a node who has
	      ; the same small and large values as the original
	      ; node, but with a list of middle values added.
	      (insert 
		(cons (getSmallVal inNode) 
		(cons (getLargeVal inNode) 
	        (cons (getSmallList inNode) 
		(cons (insert () (cons (first toInsertList) nil))
		(cons (getLargeList inNode) nil))))) 
		(cdr toInsertList)
	      ) 
	    )
	    ; if the length of the mid list is  1
	    ((>= (length (getMidList inNode)) 1)
	      ; recursively call insert on a node who has
	      ; the same small and large values as the original
	      ; node, but with a list of middle values added.
	      (insert 
		(cons (getSmallVal inNode) 
		(cons (getLargeVal inNode) 
		(cons (getSmallList inNode) 
		(cons (insert (getMidList inNode) (cons (first toInsertList) nil))
		(cons (getLargeList inNode) nil))))) 
		(cdr toInsertList)
	      ) 
	    )
	  )
	)
	; if the value to insert is larger than the 
	; large value.
	((> (first toInsertList) (getLargeVal inNode))
	  (cond 
	    ; if the length of the large list is 0
	    ((< (length (getLargeList inNode)) 1)
	      ; recursively call insert on a node who has
	      ; the same small and large values as the original
	      ; node, but with a list of large values added.
	      (insert 
		(cons (getSmallVal inNode) 
		(cons (getLargeVal inNode) 
		(cons (getSmallList inNode) 
		(cons (getMidList inNode) 
		(cons (insert () (cons (first toInsertList) nil)) nil))))) 
		(cdr toInsertList)
	      ) 
	    )
	    ; if the length of the large list is 1
	    ((>= (length (getLargeList inNode)) 1)
	      ; recursively call insert on a node who has
	      ; the same small and large values as the original
	      ; node, but with a list of large values added.
	      (insert 
		(cons (getSmallVal inNode) 
		(cons (getLargeVal inNode) 
		(cons (getSmallList inNode) 
		(cons (getMidList inNode) 
		(cons (insert (getLargeList inNode) (cons (first toInsertList) nil)) nil))))) 
		(cdr toInsertList)
	      ) 
	    )
	  )
	)
      )
    )
  )
)

; Function that drives the insert function and prints
; out after each insertion.
(defun printInsertions (inNode toInsertList)
  (print (outputForm inNode))
  (cond
    ((< (length toInsertList) 1)
      inNode
    ) 
    ((>= (length toInsertList) 1)
      (printInsertions(insert inNode (cons (car toInsertList) nil)) (cdr toInsertList))
    ) 
  )
)

; Build the ternary tree based on the list described
; in input.txt and print its status after each insertion.
(printInsertions () (read (open "inputs/input8.txt")))

